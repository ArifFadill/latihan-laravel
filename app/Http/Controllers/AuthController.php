<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function kirim(Request $request){
        $nama = $request ['nama'];
        $gander = $request ['gander'];
        $Nationality =$request ['National'];
        $Bahasa = $request ['Bahasa'];
        $Bio = $request ['Bio'];
        return view ('halaman.welcome', compact('nama','gander','Nationality','Bahasa','Bio'));
    }

    public function register(){
        return view('halaman.register');
    }
}
