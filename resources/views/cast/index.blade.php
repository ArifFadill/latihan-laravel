@extends('layout.master')

@section('judul')
    Halaman list Cast
@endsection

@section('content')
<a href="/cast/create" class="btn btn-success mb-2">Tambah Data</a>
<table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">No</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
        <th scope="col">action</th>
      </tr>
    </thead>
    <tbody>
      @forelse ($cast as $key=>$item)
        <tr>
            <td>{{$key + 1}}</td>
            <td>{{$item->nama}}</td>
            <td>{{$item->umur}}</td>
            <td>{{$item->bio}}</td>
            <td>
                <form action="/cast/{{$item->id}}" method="POST">
                <a href="/cast/{{$item->id}}" class="btn btn-info btn-sm">Detail</a>
                <a href="/cast/{{$item->id}}/edit" class="btn btn-warning btn-sm">edit</a>
                    @method('delete')
                    @csrf
                    <input type="Submit" class="btn btn-danger btn-sm" values="Delete">
                </form>
            </td>
        </tr>
          
      @empty
      <tr>
          <td>Data Masih Kosong</td>
      </tr>
          
      @endforelse
    </tbody>
  </table>
@endsection