@extends('layout.master')

@section('judul')
    Halaman edit cast {{$cast->nama}}
@endsection

@section('content')
<form action="/cast/{{$cast->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="form-group">
      <label>Nama cast</label>
      <input type="text" name="nama" values ="{{$cast->nama}}" class="form-control">
@error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    </div>
    <div class="form-group">
      <label>Umur</label>
      <input type="text" name="umur" values ="{{$cast->umur}}" class="form-control">
@error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    </div>
    <div class="form-group">
        <label>Bio</label>
        <textarea name="bio" class="form-control" cols="30" rows="10"> {{$cast->bio}}</textarea>
 @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
@enderror
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>

@endsection