<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'indexController@home');

Route::get('/register', 'AuthController@register');

Route::post('/welcome', 'AuthController@kirim');

Route::get('/table-data', function(){
    return view('table.table-data');
});

//CRUD CAST
Route::get('/cast/create', 'castController@create');

Route::POST('/cast', 'CastController@store');

Route::get('/cast', 'castController@index');

Route::get('/cast/{cast_id}', 'castController@show');

Route::get('/cast/{cast_id}/edit', 'castController@edit');

Route::put('/cast/{cast_id}', 'CastController@update');

Route::delete('/cast/{cast_id}', 'CastController@destroy');